package com.chb.design.singleton;

/**
 * @ClassName App
 * @Description 测试
 * @Author chenhongbao
 * @Date 2023/4/13 15:26
 * @Version 1.0
 */
public class App {


    public static void main(String[] args) {
        //饿汉
//        testHungryContext();

        //懒汉
//        testLazyContext();

        //静态内部类
//        testStaticNestedContext();

        //枚举
        testEnumContext();

    }




    /**
     * 测试饿汉
     */
    private static void testHungryContext() {
        HungryContext instance = HungryContext.getInstance();
        HungryContext instance1 = HungryContext.getInstance();
        HungryContext instance2 = HungryContext.getInstance();

        System.out.println(instance);
        System.out.println(instance1);
        System.out.println(instance2);
    }

    /**
     * 测试懒汉
     */
    private static void testLazyContext() {
        LazyContext instance = LazyContext.getInstance();
        LazyContext instance1 = LazyContext.getInstance();
        LazyContext instance2 = LazyContext.getInstance();

        System.out.println(instance);
        System.out.println(instance1);
        System.out.println(instance2);
    }


    /**
     * 测试静态内部类
     */
    private static void testStaticNestedContext() {

        StaticNestedContext instance = StaticNestedContext.getInstance();
        StaticNestedContext instance1 = StaticNestedContext.getInstance();
        StaticNestedContext instance2 = StaticNestedContext.getInstance();

        System.out.println(instance);
        System.out.println(instance1);
        System.out.println(instance2);
    }

    /**
     * 测试枚举
     */
    private static void testEnumContext() {
        String name1 = EnumContext.getNameByType("1");
        System.out.println(name1);

        String name2 = EnumContext.getNameByType("1");
        System.out.println(name2);
    }
}
