package com.chb.design.singleton;

/**
 * @ClassName EnumContext
 * @Description 单例 - 枚举值
 * @Author chenhongbao
 * @Date 2023/4/13 17:11
 * @Version 1.0
 */
public enum EnumContext {
    sign("0", "evoucher.sign.signCertID"),
    envelop("1", "evoucher.sign.envelopCertID"),
    sm2Sign("2", "evoucher.sign.SM2_SignCertID"),
    sm2Envelop("3", "evoucher.sign.SM2_envelopCertID");

    private String certType;
    private String certName;

    public String getCertType() {
        return certType;
    }

    public String getCertName() {
        return certName;
    }


    EnumContext(String certType, String certName) {
        this.certType = certType;
        this.certName = certName;
    }

    public static String getNameByType(String certType) {
        for (EnumContext enumContext : EnumContext.values()) {
            if (certType.equalsIgnoreCase(enumContext.getCertType())) {
                return enumContext.getCertName();
            }
        }
        return "";
    }
}
