package com.chb.design.singleton;

/**
 * 单例模式——饿汉，传统说法是需要配置文件之类的不太方便,个人觉得如果不需要在getInstance传递参数其实无所谓
 *
 * @author randaliang
 * @date 2021‐09‐20 9:02
 **/
public class HungryContext {
    private static  HungryContext instance = new HungryContext();

    public static HungryContext getInstance(){
        return instance;
    }

    public HungryContext() {
    }
}
