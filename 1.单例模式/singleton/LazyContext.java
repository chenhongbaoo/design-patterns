package com.chb.design.singleton;

/**
 * @ClassName LazyContext
 * @Description 饿汉-双重锁
 * @Author chenhongbao
 * @Date 2023/4/13 15:47
 * @Version 1.0
 */
public class LazyContext {
    private volatile static LazyContext instance;

    private LazyContext() {
    }

    public static LazyContext getInstance() {
        if (instance == null) {
            synchronized (LazyContext.class) {
                if (instance == null) {
                    instance = new LazyContext();
                }
            }
        }
        return instance;
    }
}
