package com.chb.design.singleton;

/**
 * @ClassName StaticNestedContext
 * @Description 单例 - 静态内部类模式
 * @Author chenhongbao
 * @Date 2023/4/13 17:03
 * @Version 1.0
 */
public class StaticNestedContext {
    private static class SingletonHolder{
        private static final StaticNestedContext instance = new StaticNestedContext();
    }

    private StaticNestedContext() {
    }

    public static StaticNestedContext getInstance(){
        return SingletonHolder.instance;
    }
}
