package com.chb.design.prototype;

/**
 * 意图：用原型实例指定创建对象的种类，并且通过拷贝这些原型创建新的对象。
 *
 * 主要解决：在运行期建立和删除原型。
 *
 * 如何解决：利用已有的一个原型对象，快速地生成和原型对象一样的实例。
 *
 * 优点： 1、性能提高。 2、逃避构造函数的约束。
 *
 * 缺点： 1、配备克隆方法需要对类的功能进行通盘考虑，这对于全新的类不是很难，
 *          但对于已有的类不一定很容易，特别当一个类引用不支持串行化的间接对象，或者引用含有循环结构的时候。
 *       2、必须实现 Cloneable 接口。
 *
 */

/**
 * @ClassName App
 * @Description 原型模式
 * @Author chenhongbao
 * @Date 2023/7/5 9:33
 * @Version 1.0
 */
public class App {
    public static void main(String[] args) throws CloneNotSupportedException {
        ShapeCash.loadCache();

        Shape shape1 = ShapeCash.getShape("1");
        System.out.println(shape1.getType());

        Shape shape2 = ShapeCash.getShape("2");
        System.out.println(shape2.getType());

        Shape shape3 = ShapeCash.getShape("3");
        System.out.println(shape3.getType());
    }
}
