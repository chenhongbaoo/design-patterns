package com.chb.design.prototype;

/**
 * @ClassName Circle
 * @Description 步骤 2 创建扩展了上面抽象类的实体类
 * @Author chenhongbao
 * @Date 2023/7/5 9:42
 * @Version 1.0
 */
public class Circle extends Shape {
    public Circle() {
        type = "Circle";
    }

}
