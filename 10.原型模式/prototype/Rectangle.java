package com.chb.design.prototype;

/**
 * @ClassName Rectangle
 * @Description 步骤 2 创建扩展了上面抽象类的实体类
 * @Author chenhongbao
 * @Date 2023/7/5 9:40
 * @Version 1.0
 */
public class Rectangle extends Shape{
    public Rectangle() {
        type = "Rectangle";
    }

}
