package com.chb.design.prototype;

/**
 * @ClassName Shape
 * @Description 步骤 1 创建一个实现了 Cloneable 接口的抽象类
 * @Author chenhongbao
 * @Date 2023/7/5 9:35
 * @Version 1.0
 */
public abstract class Shape implements Cloneable{
    private String id;
    protected String type;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Object clone = null;
        try {
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
}
