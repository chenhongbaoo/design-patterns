package com.chb.design.prototype;

import java.util.Hashtable;

/**
 * @ClassName ShapeCash
 * @Description 步骤 3 创建一个类，从数据库获取实体类，并把它们存储在一个 Hashtable 中。
 * @Author chenhongbao
 * @Date 2023/7/5 9:43
 * @Version 1.0
 */
public class ShapeCash {
    private static Hashtable<String,Shape> shapeMap = new Hashtable<>();

    public static Shape getShape(String shapeId) throws CloneNotSupportedException {
        Shape cacheShape = shapeMap.get(shapeId);
        return (Shape) cacheShape.clone();
    }

    // 对每种形状都运行数据库查询，并创建该形状
    // shapeMap.put(shapeKey, shape);
    // 例如，我们要添加三种形状
    public static void loadCache() {
        Circle circle = new Circle();
        circle.setId("1");
        shapeMap.put(circle.getId(),circle);

        Square square = new Square();
        square.setId("2");
        shapeMap.put(square.getId(),square);

        Rectangle rectangle = new Rectangle();
        rectangle.setId("3");
        shapeMap.put(rectangle.getId(),rectangle);
    }
}
