package com.chb.design.facade;

/**
 * @ClassName Square
 * @Description 步骤 2 创建实现接口的实体类。
 * @Author chenhongbao
 * @Date 2023/7/7 10:26
 * @Version 1.0
 */
public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Circle draw ...");
    }
}
