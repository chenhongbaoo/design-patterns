package com.chb.design.facade;

/**
 * @ClassName Rectangle
 * @Description 步骤 2 创建实现接口的实体类。
 * @Author chenhongbao
 * @Date 2023/7/7 10:25
 * @Version 1.0
 */
public class Rectangle implements Shape{
    @Override
    public void draw() {
        System.out.println("Rectangle draw ...");
    }
}
