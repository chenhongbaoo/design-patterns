package com.chb.design.facade;

/**
 * @ClassName Shape
 * @Description 步骤 1 创建一个接口。
 * @Author chenhongbao
 * @Date 2023/7/7 10:25
 * @Version 1.0
 */
public interface Shape {
    void draw();
}
