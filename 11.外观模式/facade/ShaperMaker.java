package com.chb.design.facade;

/**
 * @ClassName ShaperMaker
 * @Description 步骤 3 创建一个外观类
 * @Author chenhongbao
 * @Date 2023/7/7 10:26
 * @Version 1.0
 */
public class ShaperMaker {
    private Circle circle;
    private Rectangle rectangle;
    private Square square;

    public ShaperMaker() {
        circle = new Circle();
        rectangle = new Rectangle();
        square = new Square();
    }

    public void drawCircle(){
        circle.draw();
    }

    public void drawRectangle(){
        rectangle.draw();
    }

    public void drawSquare(){
        square.draw();
    }

}
