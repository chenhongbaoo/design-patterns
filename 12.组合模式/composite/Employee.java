package com.chb.design.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Employee
 * @Description 步骤 1 创建 Employee 类，该类带有 Employee 对象的列表
 * @Author chenhongbao
 * @Date 2023/7/7 11:05
 * @Version 1.0
 */
public class Employee {
    private String name;
    private String dept;
    private int salary;
    //下级
    private List<Employee> sub;


    public Employee(String name, String dept, int salary) {
        this.name = name;
        this.dept = dept;
        this.salary = salary;
        sub = new ArrayList<>();
    }

    public void add(Employee employee){
        sub.add(employee);
    }

    public void remove(Employee employee){
        sub.remove(employee);
    }

    public List<Employee> getSub(){
        return sub;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", dept='" + dept + '\'' +
                ", salary=" + salary +
                ", sub=" + sub +
                '}';
    }
}
