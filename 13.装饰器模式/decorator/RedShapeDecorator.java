package com.chb.design.decorator;

/**
 * @ClassName RedShapeDecorator
 * @Description 步骤 4 创建扩展了 ShapeDecorator 类的实体装饰类。
 * @Author chenhongbao
 * @Date 2023/7/7 11:24
 * @Version 1.0
 */
public class RedShapeDecorator extends ShapeDecorator{

    public RedShapeDecorator(Shape shape) {
        super(shape);
    }

    @Override
    public void draw() {
        shape.draw();
        setRedBorder(shape);
    }

    private void setRedBorder(Shape decoratedShape){
        System.out.println("Border Color: Red");
    }
}
