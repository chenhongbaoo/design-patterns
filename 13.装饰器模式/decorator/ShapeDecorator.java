package com.chb.design.decorator;

/**
 * @ClassName ShapeDecorator
 * @Description 步骤 3 创建实现了 Shape 接口的抽象装饰类。
 * @Author chenhongbao
 * @Date 2023/7/7 11:23
 * @Version 1.0
 */
public class ShapeDecorator implements Shape {

    protected Shape shape;

    public ShapeDecorator(Shape shape) {
        this.shape = shape;
    }

    @Override
    public void draw() {

    }
}
