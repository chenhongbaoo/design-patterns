package com.chb.design.observer;

/**
 * @ClassName BinaryObserver
 * @Description 步骤 3 创建实体观察者类。
 * @Author chenhongbao
 * @Date 2023/7/31 14:35
 * @Version 1.0
 */
public class BinaryObserver extends Observer{
    public BinaryObserver(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println( "Binary String: "
                + Integer.toBinaryString( subject.getState() ) );
    }
}
