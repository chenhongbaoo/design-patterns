package com.chb.design.observer;

/**
 * @ClassName HexaObserver
 * @Description 步骤 3 创建实体观察者类。
 * @Author chenhongbao
 * @Date 2023/7/31 14:38
 * @Version 1.0
 */
public class HexaObserver extends Observer{

    public HexaObserver(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println( "Hex String: "
                + Integer.toHexString( subject.getState() ).toUpperCase() );
    }
}