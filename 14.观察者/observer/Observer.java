package com.chb.design.observer;

/**
 * @ClassName Observer
 * @Description 步骤 2 创建 Observer 类。
 * @Author chenhongbao
 * @Date 2023/7/31 14:35
 * @Version 1.0
 */
public abstract class  Observer {
    protected Subject subject;
    public abstract void update();
}
