package com.chb.design.observer;

/**
 * @ClassName OctalObserver
 * @Description 步骤 3 创建实体观察者类。
 * @Author chenhongbao
 * @Date 2023/7/31 14:37
 * @Version 1.0
 */
public class OctalObserver extends Observer{
    public OctalObserver(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println( "Octal String: "
                + Integer.toOctalString( subject.getState() ) );
    }
}
