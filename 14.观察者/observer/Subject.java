package com.chb.design.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Subject
 * @Description 步骤 1 创建 Subject 类。  主题,也称为被观察者或可观察者
 * @Author chenhongbao
 * @Date 2023/7/31 13:54
 * @Version 1.0
 */
public class Subject {
    private List<Observer> observers = new ArrayList<Observer>();


    private int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
        notifyAllObservers();
    }


    public void attach(Observer observer){
        observers.add(observer);
    }


    public void notifyAllObservers(){
        for (Observer observer : observers) {
            observer.update();
        }
    }



}
