package com.chb.design.interpreter;

/**
 * @ClassName OrExpression
 * @Description 步骤 2 创建实现了上述接口的实体类
 * @Author chenhongbao
 * @Date 2023/7/31 16:19
 * @Version 1.0
 */
public class AndExpression implements Expression {
    private Expression expression1 = null;
    private Expression expression2 = null;

    public AndExpression(Expression expression1, Expression expression2) {
        this.expression1 = expression1;
        this.expression2 = expression2;
    }

    @Override
    public boolean interpret(String context) {
        return expression1.interpret(context) && expression1.interpret(context);
    }
}
