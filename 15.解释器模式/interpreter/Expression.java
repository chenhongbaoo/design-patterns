package com.chb.design.interpreter;

/**
 * @ClassName Expression
 * @Description 步骤 1 创建一个表达式接口
 * @Author chenhongbao
 * @Date 2023/7/31 15:03
 * @Version 1.0
 */
public interface Expression {
     boolean interpret(String context);
}
