package com.chb.design.interpreter;

/**
 * @ClassName TerminaExpression
 * @Description 步骤 2 创建实现了上述接口的实体类
 * @Author chenhongbao
 * @Date 2023/7/31 16:18
 * @Version 1.0
 */
public class TerminalExpression implements Expression{

    private String data;

    public TerminalExpression(String data) {
        this.data = data;
    }

    @Override
    public boolean interpret(String context) {
        if(context.contains(data)){
            return true;
        }
        return false;
    }
}
