package com.chb.design.mediator;

import java.util.Date;

/**
 * @ClassName ChatRoom
 * @Description 步骤 1 创建中介类
 * @Author chenhongbao
 * @Date 2023/9/11 16:05
 * @Version 1.0
 */
public class ChatRoom {
    public static void showMessage(User user,String message){
        System.out.println(new Date().toString()+" ["+user.getName()+"] 说："+message);
    }
}
