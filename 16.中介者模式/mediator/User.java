package com.chb.design.mediator;

import lombok.Data;

/**
 * @ClassName User
 * @Description 步骤 2 创建 user 类
 * @Author chenhongbao
 * @Date 2023/9/11 16:06
 * @Version 1.0
 */
@Data
public class User {
    private String name;

    public User(String name) {
        this.name = name;
    }

    public void sendMessage(String message) {
        ChatRoom.showMessage(this, message);
    }
}
