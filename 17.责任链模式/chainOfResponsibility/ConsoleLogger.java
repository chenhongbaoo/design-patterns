package com.chb.design.chainOfResponsibility;

/**
 * @ClassName ConsoleLogger
 * @Description 步骤 2 创建扩展了该记录器类的实体类。
 * @Author chenhongbao
 * @Date 2023/9/11 16:32
 * @Version 1.0
 */
public class ConsoleLogger extends AbstractLogger{

    public ConsoleLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("Standard Console::Logger: " + message);
    }
}
