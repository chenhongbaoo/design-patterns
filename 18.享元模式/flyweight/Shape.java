package com.chb.design.flyweight;

/**
 * @ClassName Shape
 * @Description 步骤 1 创建一个接口。
 * @Author chenhongbao
 * @Date 2023/10/10 16:51
 * @Version 1.0
 */
public interface Shape {
    void draw();
}
