package com.chb.design.flyweight;

import java.util.HashMap;

/**
 * @ClassName ShapeFactory
 * @Description 步骤 3 创建一个工厂，生成基于给定信息的实体类的对象。
 * @Author chenhongbao
 * @Date 2023/10/10 16:54
 * @Version 1.0
 */
public class ShapeFactory {
    private static final HashMap<String, Shape> circleMap = new HashMap<>();

    public static Shape getCircle(String color){
        Circle circle = (Circle) circleMap.get(color);

        //只创建一次，如果创建过直接使用，没创建就创建一个
        if(null == circle){
            circle = new Circle(color);
            circleMap.put(color,circle);
            System.out.println("create circle of color:"+color);
        }
        return circle;
    }

}
