package com.chb.design.command;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Broker
 * @Description 步骤 4 创建命令调用类。
 * @Author chenhongbao
 * @Date 2023/10/27 9:42
 * @Version 1.0
 */
public class Broker {

    private List<Order> orderList = new ArrayList<>();

    public void takeOrder(Order order){
        orderList.add(order);
    }

    public void palaceOrders(){
        for (Order order : orderList) {
            order.execute();
        }
        orderList.clear();
    }

}
