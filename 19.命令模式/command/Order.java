package com.chb.design.command;

/**
 * @ClassName Order
 * @Description 步骤 1 创建一个命令接口
 * @Author chenhongbao
 * @Date 2023/10/27 9:32
 * @Version 1.0
 */
public interface Order {
    void execute();
}
