package com.chb.design.command;

/**
 * @ClassName BuyStock
 * @Description 步骤 3 创建实现了 Order 接口的实体类。
 * @Author chenhongbao
 * @Date 2023/10/27 9:37
 * @Version 1.0
 */
public class SellStock implements  Order {

    private Stock abcStock;

    public SellStock(Stock abcStock){
        this.abcStock = abcStock;
    }

    @Override
    public void execute() {
        abcStock.sell();
    }
}
