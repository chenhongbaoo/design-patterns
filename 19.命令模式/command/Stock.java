package com.chb.design.command;

/**
 * @ClassName Stock
 * @Description 步骤 2 创建一个请求类
 * @Author chenhongbao
 * @Date 2023/10/27 9:35
 * @Version 1.0
 */
public class Stock {
    private String name = "ABC";
    private int quantity = 10;

    public void buy() {
        System.out.println("Stock [ name:" + name + ",Quantity:" + quantity + "] bought");
    }

    public void sell() {
        System.out.println("Stock [ name:" + name + ",Quantity:" + quantity + "] sold");
    }

}
