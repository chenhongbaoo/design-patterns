package com.chb.design.factory;

import com.chb.design.factory.inter.Shape;

/**
 * @ClassName App
 * @Description 简单工厂 - 测试
 * @Author chenhongbao
 * @Date 2023/4/19 11:46
 * @Version 1.0
 */
public class App {
    public static void main(String[] args) {
        ShapeFactory shapeFactory = new ShapeFactory();
        Shape shape = shapeFactory.getShape("Rectangle");
        shape.draw();
    }
}
