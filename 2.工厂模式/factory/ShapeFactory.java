package com.chb.design.factory;

import com.chb.design.factory.impl.Circle;
import com.chb.design.factory.impl.Rectangle;
import com.chb.design.factory.impl.Square;
import com.chb.design.factory.inter.Shape;

/**
 * @ClassName ShapeFactory
 * @Description 简单工厂
 * @Author chenhongbao
 * @Date 2023/4/19 11:44
 * @Version 1.0
 */
public class ShapeFactory {
    public Shape getShape(String type) {
        if (null == type || "".equals(type)) {
            return null;
        }
        if ("Rectangle".equalsIgnoreCase(type)) {
            return new Rectangle();
        } else if ("Square".equalsIgnoreCase(type)) {
            return new Square();
        } else if ("Circle".equalsIgnoreCase(type)) {
            return new Circle();
        }
        return null;

    }
}
