package com.chb.design.factory.impl;

import com.chb.design.factory.inter.Shape;

/**
 * @ClassName Rectangle
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/4/19 11:42
 * @Version 1.0
 */
public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("画一个长方形");
    }
}
