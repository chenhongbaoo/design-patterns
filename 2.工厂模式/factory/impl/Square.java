package com.chb.design.factory.impl;

import com.chb.design.factory.inter.Shape;

/**
 * @ClassName Square
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/4/19 11:43
 * @Version 1.0
 */
public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("画一个正方形");
    }
}
