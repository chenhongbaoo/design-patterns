package com.chb.design.factory.inter;

/**
 * @ClassName Shape
 * @Description 简单工厂 - 接口
 * @Author chenhongbao
 * @Date 2023/4/19 11:41
 * @Version 1.0
 */
public interface Shape {
    void draw();
}
