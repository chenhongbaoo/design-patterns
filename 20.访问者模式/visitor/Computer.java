package com.chb.design.visitor;

/**
 * @ClassName Keyboard
 * @Description 步骤 2 创建扩展了上述类的实体类
 * @Author chenhongbao
 * @Date 2023/11/3 10:24
 * @Version 1.0
 */
public class Computer implements ComputerPart {

    ComputerPart[] parts ;

    public Computer() {
        parts = new ComputerPart[]{new Keyboard(),new Monitor(),new Mouse()};
    }

    @Override
    public void accept(ComputerPartVisitor computerPartVisitor) {
        for (int i = 0; i < parts.length; i++) {
            parts[i].accept(computerPartVisitor);
        }

        computerPartVisitor.visit(this);
    }
}
