package com.chb.design.visitor;

/**
 * @ClassName ComputerPart
 * @Description 步骤 1 定义一个表示元素的接口
 * @Author chenhongbao
 * @Date 2023/11/3 10:22
 * @Version 1.0
 */
public interface ComputerPart {
    void accept(ComputerPartVisitor computerPartVisitor);
}
