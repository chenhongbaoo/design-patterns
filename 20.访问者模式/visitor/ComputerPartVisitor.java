package com.chb.design.visitor;

/**
 * @ClassName ComputerPartVisitor
 * @Description 步骤 3 定义一个表示访问者的接口。
 * @Author chenhongbao
 * @Date 2023/11/3 10:24
 * @Version 1.0
 */
public interface ComputerPartVisitor {
    void visit(Computer computer);

    void visit(Mouse mouse);

    void visit(Keyboard keyboard);

    void visit(Monitor monitor);
}
