package com.chb.design.visitor;

/**
 * @ClassName ComputerpartDisplatVisitor
 * @Description 步骤 4 创建实现了上述类的实体访问者
 * @Author chenhongbao
 * @Date 2023/11/3 10:30
 * @Version 1.0
 */
public class ComputerpartDisplatVisitor implements ComputerPartVisitor {
    @Override
    public void visit(Computer computer) {
        System.out.println("Display Computer");
    }

    @Override
    public void visit(Mouse mouse) {
        System.out.println("Display Mouse");

    }

    @Override
    public void visit(Keyboard keyboard) {
        System.out.println("Display Keyboard");

    }

    @Override
    public void visit(Monitor monitor) {
        System.out.println("Display Monitor");

    }
}
