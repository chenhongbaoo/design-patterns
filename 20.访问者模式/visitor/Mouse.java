package com.chb.design.visitor;

/**
 * @ClassName Keyboard
 * @Description 步骤 2 创建扩展了上述类的实体类
 * @Author chenhongbao
 * @Date 2023/11/3 10:24
 * @Version 1.0
 */
public class Mouse implements ComputerPart {
    @Override
    public void accept(ComputerPartVisitor computerPartVisitor) {
        computerPartVisitor.visit(this);
    }
}
