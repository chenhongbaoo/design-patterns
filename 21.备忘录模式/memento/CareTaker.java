package com.chb.design.memento;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName CareTaker
 * @Description 步骤3  创建CareTaker
 * @Author chenhongbao
 * @Date 2023/11/21 16:42
 * @Version 1.0
 */
public class CareTaker {
    private List<Memento> mementoList  = new ArrayList<>();

    public void add(Memento state){
        mementoList.add(state);
    }

    public Memento get(int index){
        return mementoList.get(index);
    }



}
