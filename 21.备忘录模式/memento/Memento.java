package com.chb.design.memento;

/**
 * @ClassName Memento
 * @Description 步骤 1 创建 Memento 类。
 * @Author chenhongbao
 * @Date 2023/11/21 16:39
 * @Version 1.0
 */
public class Memento {
    private String state;

    public Memento(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
