package com.chb.design.memento;

/**
 * @ClassName Originator
 * @Description 步骤2 创建Originator
 * @Author chenhongbao
 * @Date 2023/11/21 16:40
 * @Version 1.0
 */
public class Originator {
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Memento saveStateToMemento(){
        return new Memento(state);
    }

    public void getStateFromMemento(Memento memento){
        state = memento.getState();
    }
}
