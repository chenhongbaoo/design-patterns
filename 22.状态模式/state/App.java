package com.chb.design.state;

/**
 * @ClassName App
 * @Description 状态模式
 * @Author chenhongbao
 * @Date 2023/11/23 14:05
 * @Version 1.0
 */
public class App {
    public static void main(String[] args) {
        Context context = new Context();

        StartState startState = new StartState();
        startState.doAction(context);

        System.out.println(context.getState().toString());

        StopState stopState = new StopState();
        stopState.doAction(context);

        System.out.println(context.getState().toString());
    }
}
