package com.chb.design.state;

/**
 * @ClassName Context
 * @Description 步骤 4 使用 Context 来查看当状态 State 改变时的行为变化。
 * @Author chenhongbao
 * @Date 2023/12/15 17:33
 * @Version 1.0
 */
public class Context {
    private State state;

    public Context(){
        state = null;
    }

    public void setState(State state){
        this.state = state;
    }

    public State getState(){
        return state;
    }
}
