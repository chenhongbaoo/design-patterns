package com.chb.design.state;

/**
 * @ClassName StartState
 * @Description 步骤 2 创建实现接口的实体类
 * @Author chenhongbao
 * @Date 2023/12/15 17:33
 * @Version 1.0
 */
public class StartState implements State{
    public void doAction(Context context) {
        System.out.println("Player is in start state");
        context.setState(this);
    }

    public String toString(){
        return "Start State";
    }
}
