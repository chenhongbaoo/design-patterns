package com.chb.design.state;

/**
 * @ClassName State
 * @Description 步骤 1 创建一个接口。
 * @Author chenhongbao
 * @Date 2023/12/15 17:32
 * @Version 1.0
 */
public interface State {
    public void doAction(Context context);
}
