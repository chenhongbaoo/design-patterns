package com.chb.design.state;

/**
 * @ClassName StopState
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/12/15 17:33
 * @Version 1.0
 */
public class StopState implements State {

    public void doAction(Context context) {
        System.out.println("Player is in stop state");
        context.setState(this);
    }

    public String toString(){
        return "Stop State";
    }
}
