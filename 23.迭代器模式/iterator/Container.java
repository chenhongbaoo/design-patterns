package com.chb.design.iterator;

/**
 * @ClassName Container
 * @Description TODO
 * @Author chenhongbao
 * @Date 2024/1/3 17:20
 * @Version 1.0
 */
public interface Container {
     Iterator getIterator();
}
