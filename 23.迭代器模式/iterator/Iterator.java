package com.chb.design.iterator;

/**
 * @ClassName Iterator
 * @Description TODO
 * @Author chenhongbao
 * @Date 2024/1/3 17:19
 * @Version 1.0
 */
public interface Iterator {
    boolean hasNext ();

    Object next ();
}
