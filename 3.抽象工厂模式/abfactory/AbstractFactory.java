package com.chb.design.abfactory;

import com.chb.design.abfactory.color.inter.Color;
import com.chb.design.abfactory.shape.inter.Shape;

/**
 * @ClassName AbstractFactory
 * @Description 形状shape和颜色color的抽象类
 * @Author chenhongbao
 * @Date 2023/4/21 15:03
 * @Version 1.0
 */
public abstract class AbstractFactory {
    public abstract Color getColor(String color);
    public abstract Shape getShape(String shape);
}
