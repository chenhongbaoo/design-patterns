package com.chb.design.abfactory;

import com.chb.design.abfactory.color.inter.Color;
import com.chb.design.abfactory.shape.inter.Shape;

/**
 * @ClassName App
 * @Description 抽象工厂
 * @Author chenhongbao
 * @Date 2023/4/21 9:05
 * @Version 1.0
 */
public class App {
    public static void main(String[] args) {
        AbstractFactory factory = FactoryProducer.getFactory("color");
        Color color = factory.getColor("red");
        color.fill();

        AbstractFactory shape = FactoryProducer.getFactory("shape");
        Shape circle = shape.getShape("circle");
        circle.draw();


    }
}
