package com.chb.design.abfactory;

import com.chb.design.abfactory.color.ColorFactory;
import com.chb.design.abfactory.shape.ShapeFactory;

/**
 * @ClassName FactoryProducer
 * @Description 创建一个工厂创造器/生成器类，通过传递形状或颜色信息来获取工厂。
 * @Author chenhongbao
 * @Date 2023/4/21 15:30
 * @Version 1.0
 */
public class FactoryProducer {
    /**
     * 工厂生成
     *
     * @param choice 形状还是颜色 SHAPE COLOR
     * @return
     */
    public static AbstractFactory getFactory(String choice) {
        if (choice.equalsIgnoreCase("SHAPE")) {
            return new ShapeFactory();
        } else if (choice.equalsIgnoreCase("COLOR")) {
            return new ColorFactory();
        }
        return null;
    }
}
