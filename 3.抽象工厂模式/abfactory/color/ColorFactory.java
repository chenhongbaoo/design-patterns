package com.chb.design.abfactory.color;

import com.chb.design.abfactory.AbstractFactory;
import com.chb.design.abfactory.color.impl.Green;
import com.chb.design.abfactory.color.impl.Red;
import com.chb.design.abfactory.color.inter.Color;
import com.chb.design.abfactory.shape.inter.Shape;

/**
 * @ClassName ColorFactory
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/4/21 15:29
 * @Version 1.0
 */
public class ColorFactory extends AbstractFactory {
    @Override
    public Color getColor(String color) {
        if(color == null){
            return null;
        }
        if(color.equalsIgnoreCase("RED")){
            return new Red();
        } else if(color.equalsIgnoreCase("GREEN")){
            return new Green();
        }
        return null;
    }

    @Override
    public Shape getShape(String shape) {
        return null;
    }
}
