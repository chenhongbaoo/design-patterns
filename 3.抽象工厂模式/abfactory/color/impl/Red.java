package com.chb.design.abfactory.color.impl;

import com.chb.design.abfactory.color.inter.Color;

/**
 * @ClassName Green
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/4/21 15:05
 * @Version 1.0
 */
public class Red implements Color {
    @Override
    public void fill() {
        System.out.println("涂上红色");
    }
}
