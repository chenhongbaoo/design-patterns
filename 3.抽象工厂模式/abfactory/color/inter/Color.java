package com.chb.design.abfactory.color.inter;

/**
 * @ClassName Color
 * @Description 颜色 - 接口
 * @Author chenhongbao
 * @Date 2023/4/21 15:04
 * @Version 1.0
 */
public interface Color {
    void fill();
}
