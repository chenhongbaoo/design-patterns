package com.chb.design.abfactory.shape;

import com.chb.design.abfactory.AbstractFactory;
import com.chb.design.abfactory.color.inter.Color;
import com.chb.design.abfactory.shape.impl.Circle;
import com.chb.design.abfactory.shape.impl.Rectangle;
import com.chb.design.abfactory.shape.impl.Square;
import com.chb.design.abfactory.shape.inter.Shape;

/**
 * @ClassName ShapeFactory
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/4/21 15:27
 * @Version 1.0
 */
public class ShapeFactory extends AbstractFactory {
    @Override
    public Color getColor(String color) {
        return null;
    }

    @Override
    public Shape getShape(String shape) {
        if(shape == null){
            return null;
        }
        if(shape.equalsIgnoreCase("CIRCLE")){
            return new Circle();
        } else if(shape.equalsIgnoreCase("RECTANGLE")){
            return new Rectangle();
        } else if(shape.equalsIgnoreCase("SQUARE")){
            return new Square();
        }
        return null;
    }
}
