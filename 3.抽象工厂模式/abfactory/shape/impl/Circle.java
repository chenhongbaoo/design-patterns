package com.chb.design.abfactory.shape.impl;


import com.chb.design.abfactory.shape.inter.Shape;

/**
 * @ClassName Circle
 * @Description 圆 - 实体类
 * @Author chenhongbao
 * @Date 2023/4/19 11:44
 * @Version 1.0
 */
public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("画一个原");
    }
}
