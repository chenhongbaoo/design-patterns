package com.chb.design.abfactory.shape.impl;


import com.chb.design.abfactory.shape.inter.Shape;

/**
 * @ClassName Rectangle
 * @Description 长方形 - 实体类
 * @Author chenhongbao
 * @Date 2023/4/19 11:42
 * @Version 1.0
 */
public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("画一个长方形");
    }
}
