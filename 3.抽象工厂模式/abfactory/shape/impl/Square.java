package com.chb.design.abfactory.shape.impl;


import com.chb.design.abfactory.shape.inter.Shape;

/**
 * @ClassName Square
 * @Description 正方形 - 实体类
 * @Author chenhongbao
 * @Date 2023/4/19 11:43
 * @Version 1.0
 */
public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("画一个正方形");
    }
}
