package com.chb.design.abfactory.shape.inter;

/**
 * @ClassName Shape
 * @Description 为形状创建一个接口
 * @Author chenhongbao
 * @Date 2023/4/21 11:38
 * @Version 1.0
 */
public interface Shape {
    void draw();
}
