package com.chb.design.template;

/**
 * @ClassName App
 * @Description 模板模式
 * @Author chenhongbao
 * @Date 2023/5/9 9:26
 * @Version 1.0
 */
public class App {
    public static void main(String[] args) {
        Game game = new Football();
        game.play();
    }
}
