package com.chb.design.template;

/**
 * @ClassName Basketball
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/5/9 9:29
 * @Version 1.0
 */
public class Basketball extends Game {

    @Override
    void initialize() {
        System.out.println("Basketball initialize");
    }

    @Override
    void startPlay() {
        System.out.println("Basketball startPlay");

    }

    @Override
    void endPlay() {
        System.out.println("Basketball endPlay");

    }
}
