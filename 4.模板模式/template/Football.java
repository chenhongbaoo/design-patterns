package com.chb.design.template;

/**
 * @ClassName Basketball
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/5/9 9:29
 * @Version 1.0
 */
public class Football extends Game {

    @Override
    void initialize() {
        System.out.println("Football initialize");
    }

    @Override
    void startPlay() {
        System.out.println("Football startPlay");

    }

    @Override
    void endPlay() {
        System.out.println("Football endPlay");

    }
}
