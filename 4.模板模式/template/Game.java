package com.chb.design.template;

/**
 * @ClassName Game
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/5/9 9:26
 * @Version 1.0
 */
public abstract class Game {
    abstract void initialize();

    abstract void startPlay();

    abstract void endPlay();

    /**
     * 模板
     */
    public final void play() {
        //初始化
        initialize();
        //开始
        startPlay();
        //结束
        endPlay();
    }

}
