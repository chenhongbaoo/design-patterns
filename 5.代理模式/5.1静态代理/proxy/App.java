package com.chb.design.proxy;

/**
 * @ClassName App
 * @Description 代理模式
 * @Author chenhongbao
 * @Date 2023/5/15 18:19
 * @Version 1.0
 */
public class App {
    public static void main(String[] args) {
        Image image = new ProxyImage("文件名");

        System.out.println("第一次不走代理");
        image.display();
        System.out.println("后边开始走代理");
        image.display();
        image.display();
        image.display();
        image.display();
        image.display();
    }
}
