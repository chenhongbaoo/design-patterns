package com.chb.design.proxy;

/**
 * @ClassName Image
 * @Description 代理模式 - 接口
 * @Author chenhongbao
 * @Date 2023/5/15 18:22
 * @Version 1.0
 */
public interface Image {
     void display();
}
