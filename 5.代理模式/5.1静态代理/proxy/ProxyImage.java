package com.chb.design.proxy;

/**
 * @ClassName ProxyImage
 * @Description 代理模式 - 代理类
 * @Author chenhongbao
 * @Date 2023/5/15 18:26
 * @Version 1.0
 */
public class ProxyImage implements Image {
    private RealImage realImage;
    private String fileName;

    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public void display() {
        if (realImage == null) {
            realImage = new RealImage(fileName);
        }
        realImage.display();
    }
}
