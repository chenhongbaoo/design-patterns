package com.chb.design.proxy;

/**
 * @ClassName RealImage
 * @Description 代理模式 - 实现类
 * @Author chenhongbao
 * @Date 2023/5/15 18:22
 * @Version 1.0
 */
public class RealImage implements Image {

    private String fileName;

    public RealImage(String fileName) {
        this.fileName = fileName;
        loading();

    }

    @Override
    public void display() {
        System.out.println("display --> "+fileName);
    }

    private void loading(){
        System.out.println("loading --> "+fileName);
    }

}
