package com.chb.design.strategy;

/**
 * @ClassName App
 * @Description 策略模式
 * @Author chenhongbao
 * @Date 2023/5/24 9:20
 * @Version 1.0
 */
public class App {
    public static void main(String[] args) {
        int num1 = 5;
        int num2 = 3;

        Context context1 = new Context(new OperationAdd());
        System.out.println(context1.execute(num1,num2));;

        Context context2 = new Context(new OperationSubstract());
        System.out.println(context2.execute(num1, num2));

        Context context3 = new Context(new Operationmultiply());
        System.out.println(context3.execute(num1, num2));

    }
}
