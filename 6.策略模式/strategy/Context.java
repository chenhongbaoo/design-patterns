package com.chb.design.strategy;

/**
 * @ClassName Context
 * @Description 定义一个环境角色，提供一个计算的接口供外部使用
 * @Author chenhongbao
 * @Date 2023/5/24 9:23
 * @Version 1.0
 */
public class Context {
    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public int execute(int num1,int num2){
       return  strategy.operation(num1,num2);
    }
}
