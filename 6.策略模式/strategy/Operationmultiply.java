package com.chb.design.strategy;

/**
 * @ClassName OperationAdd
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/5/24 9:22
 * @Version 1.0
 */
public class Operationmultiply implements Strategy {
    @Override
    public int operation(int num1, int num2) {
        return num1*num2;
    }
}
