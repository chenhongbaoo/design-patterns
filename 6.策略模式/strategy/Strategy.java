package com.chb.design.strategy;

/**
 * @ClassName Strategy
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/5/24 9:20
 * @Version 1.0
 */
public interface Strategy {
    int operation(int num1,int num2);
}
