package com.chb.design.adapter.classAdapter;

/**
 * 需要被适配的对象或类型，即源码
 */
class Adaptee {
    public void method1() {
        System.out.println("classAdapter 这是原来的类，里面存在method1");
    }
}