package com.chb.design.adapter.classAdapter;

/**
 * 期待得到的目标,即适配器类
 */
class Adapter extends Adaptee implements Target {
    @Override
    public void method2() {
        System.out.println("classAdapter 这是接口中的方法。");
    }
}