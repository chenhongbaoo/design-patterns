package com.chb.design.adapter.interfaceAdapter.adapter;

/**
 * @ClassName AbstractAdapter
 * @Description 定义一个抽象类，来选择需要重写的类
 * @Author chenhongbao
 * @Date 2023/6/6 0006 10:19
 * @Version 1.0
 */
public class AbstractAdapter  implements  Target{
    @Override
    public void method1() {
        System.out.println("interfaceAdapter 默认实现 method1");
    }

    @Override
    public void method2() {
        //这个方法必须由子类来重写

    }

    @Override
    public void method3() {
        System.out.println("interfaceAdapter 默认实现 method3");
    }
}
