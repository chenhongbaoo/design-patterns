package com.chb.design.adapter.interfaceAdapter.adapter;

/**
 * @ClassName Adapter
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/6/6 0006 10:20
 * @Version 1.0
 */
public class Adapter extends AbstractAdapter {
    @Override
    public void method2() {
        System.out.println("子类实现 method2");
    }
}
