package com.chb.design.adapter.interfaceAdapter.adapter;


/**
 * @ClassName App
 * @Description 类适配器模式
 * @Author chenhongbao
 * @Date 2023/5/29 10:16
 * @Version 1.0
 */
public class App {


    public static void main(String[] args) {
        Target target = new Adapter();
        target.method1();
        target.method2();
        target.method3();

    }
}
