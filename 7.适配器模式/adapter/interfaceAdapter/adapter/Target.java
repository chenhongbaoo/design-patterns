package com.chb.design.adapter.interfaceAdapter.adapter;

/**
 * 连接目标和源的中间对象,即目标接口
 */
interface Target {
    void method1();
    void method2();
    void method3();
}