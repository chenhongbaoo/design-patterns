package com.chb.design.adapter.objectAdapter.adapter;


/**
 * 期待得到的目标,即适配器类
 */
class Adapter implements Target {

    Adaptee adaptee = new Adaptee();
    @Override
    public void method1() {
        adaptee.method1();
    }

    @Override
    public void method2() {
        System.out.println("objectAdapter 这是接口中的方法。");
    }
}