package com.chb.design.adapter.objectAdapter.adapter;

/**
 * @ClassName App
 * @Description 对象适配器模式
 * @Author chenhongbao
 * @Date 2023/6/6 0006 10:11
 * @Version 1.0
 */
public class App {
    public static void main(String[] args) {
        Target target = new Adapter();
        target.method1();
        target.method2();

    }
}
