package com.chb.design.bridge;

/**
 * @ClassName Circle
 * @Description 创建实现了 Shape 抽象类的实体类。
 * @Author chenhongbao
 * @Date 2023/6/6 0006 11:33
 * @Version 1.0
 */
public class Circle extends Shape {

    private int x;
    private int y;
    private int radius;

    public Circle(DrawAPI drawAPI, int x, int y, int radius) {
        super(drawAPI);
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    protected Circle(DrawAPI drawAPI) {
        super(drawAPI);
    }

    @Override
    public void draw() {
        drawAPI.drawCirCle(radius,x,y);
    }
}
