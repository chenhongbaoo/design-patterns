package com.chb.design.bridge;

/**
 * @ClassName DrawAPI
 * @Description 创建桥接实现接口。
 * @Author chenhongbao
 * @Date 2023/6/6 0006 11:15
 * @Version 1.0
 */
public interface DrawAPI {
    void drawCirCle(int radius,int x,int y);

}
