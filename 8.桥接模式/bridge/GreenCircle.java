package com.chb.design.bridge;

/**
 * @ClassName GreenCircle
 * @Description 创建实现了 DrawAPI 接口的实体桥接实现类。
 * @Author chenhongbao
 * @Date 2023/6/6 0006 11:21
 * @Version 1.0
 */
public class GreenCircle implements DrawAPI {
    @Override
    public void drawCirCle(int radius, int x, int y) {
        System.out.println("green circle,radius = " + radius + ",x = " + x + ",y = " + y);
    }
}
