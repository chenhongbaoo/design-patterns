package com.chb.design.bridge;

/**
 * @ClassName Sharp
 * @Description 使用 DrawAPI 接口创建抽象类 Shape。
 * @Author chenhongbao
 * @Date 2023/6/6 0006 11:27
 * @Version 1.0
 */
public abstract class Shape {
    protected DrawAPI drawAPI;

    protected Shape(DrawAPI drawAPI) {
        this.drawAPI = drawAPI;
    }

    public abstract void draw();
}
