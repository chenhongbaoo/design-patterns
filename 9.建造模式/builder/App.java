package com.chb.design.builder;

/*
 * 基本介绍：
 * 建造者模式又叫生成器模式，是一种对象构建模式。它可以将复杂对象的建造过程抽象出来（抽象类别），
 * 使这个抽象过程的不同实现方法可以构造出不同表现（属性）的对象。
 * 建造者模式是一步一步建造一个复杂对象，它允许用户只通过指定复杂对象的类型和内容就可以建造他们，
 * 用户不需要指定内部的具体构建细节。
 *
 * 建造者模式四个角色：
 * Product（产品角色）：一个具体的产品对象
 * Builder（抽象建造者）：创建一个Product对象的各个部件指定的接口/抽象类
 * ConcreteBuilder（具体建造者）：实现接口，构建和装配各个部件
 * Director（指挥者）：构建一个使用Builder的对象，它主要是用于创建一个复杂的对象，它主要有两个作用，
 *                  一是：隔离了客户与对象的生产过程;
 *                  二是：负责控制产品对象的生产过程
 *
 * 需求：
    建房子：过程为打桩、砌墙、封顶
    房子有各种各样的，比如普通房、高楼、别墅、各种房子的过程虽然一样，但是要求不要相同的
 */

/**
 * @ClassName App
 * @Description 建造模式-客户端
 * @Author chenhongbao
 * @Date 2023/7/5 9:05
 * @Version 1.0
 */
public class App {
    public static void main(String[] args) {
        CommonHouseBuilder commonHouseBuilder = new CommonHouseBuilder();
        House commonHouse = new HouseDerector(commonHouseBuilder).constractHouse();

        System.out.println("=====================");
        HighHouseBuilder highHouseBuilder = new HighHouseBuilder();
        House highHouse = new HouseDerector(highHouseBuilder).constractHouse();
    }
}
