package com.chb.design.builder;

/**
 * @ClassName CommonHouse
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/7/5 9:12
 * @Version 1.0
 */
public class CommonHouseBuilder  extends HouseBuilder {

    @Override
    public void buildBasic() {
        System.out.println("Common buildBasic...");
    }

    @Override
    public void buildWall() {
        System.out.println("Common buildWalls...");
    }

    @Override
    public void roofed() {
        System.out.println("Common roofed...");
    }
}
