package com.chb.design.builder;

/**
 * @ClassName CommonHouse
 * @Description TODO
 * @Author chenhongbao
 * @Date 2023/7/5 9:12
 * @Version 1.0
 */
public class HighHouseBuilder extends HouseBuilder {

    @Override
    public void buildBasic() {
        System.out.println("HighHouse buildBasic...");
    }

    @Override
    public void buildWall() {
        System.out.println("HighHouse buildWalls...");
    }

    @Override
    public void roofed() {
        System.out.println("HighHouse roofed...");
    }
}
