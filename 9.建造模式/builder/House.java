package com.chb.design.builder;

import lombok.Data;

/**
 * @ClassName House
 * @Description  产品House
 * @Author chenhongbao
 * @Date 2023/7/5 9:14
 * @Version 1.0
 */
@Data
public class House {
    private String baise;
    private String wall;
    private String roofed;
}
