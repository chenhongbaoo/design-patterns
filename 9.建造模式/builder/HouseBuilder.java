package com.chb.design.builder;

/**
 * @ClassName HouseBuilder
 * @Description 建造者
 * @Author chenhongbao
 * @Date 2023/7/5 9:18
 * @Version 1.0
 */
public abstract class HouseBuilder {
    protected House house=new House();
    //将建造的流程写好，抽象的方法
    public abstract void buildBasic();
    public abstract void buildWall();
    public abstract void roofed();

    //建造
    public House buildHouse(){
        return house;
    }
}
