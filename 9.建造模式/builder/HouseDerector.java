package com.chb.design.builder;

/**
 * @ClassName HouseDerector
 * @Description 指挥者 指定制作流程 返回产品
 * @Author chenhongbao
 * @Date 2023/7/5 9:17
 * @Version 1.0
 */
public class HouseDerector {
    HouseBuilder houseBuilder = null;

    //构造方法
    public HouseDerector(HouseBuilder houseBuilder) {
        this.houseBuilder = houseBuilder;
    }

    //setter 传入housebuilder
    public void setHouseBuilder(HouseBuilder houseBuilder) {
        this.houseBuilder = houseBuilder;
    }

    //如何处理建造房子的流程，交给指挥者
    public House constractHouse(){
        houseBuilder.buildBasic();
        houseBuilder.buildWall();
        houseBuilder.roofed();
        return houseBuilder.buildHouse();
    }
}
